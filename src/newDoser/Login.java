package newDoser;
import java.util.Scanner;
import javax.lang.model.type.ErrorType;
import newDoser.Control.Move;
import newDoser.Model.Book;
import newDoser.Model.DB;
import newDoser.Model.User;
import newDoser.View.View;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.lang.Thread.State;
import java.sql.ResultSetMetaData;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import java.util.Calendar;

public class Login {
	Start start = new  Start ();
	DB Dbcon =new DB();
	Move move=new Move();
	View VW=new View();
	Book BK=new Book();
	
	 
	 ArrayList<Book> BookListFile = new ArrayList<Book>();
	 ArrayList<User> UserListFile = new ArrayList<User>();
	 
	 
	
	Scanner In =new Scanner(System.in); 
	ResultSet rs=null;
	
	public void UserSystem() 
	{
	
		try {
		String ID=VW.LogID();
		String PW=VW.LogPW();
		rs=Dbcon.LogCheck(ID,PW);
		
		if(rs.next())
		{
			UserSystem2(ID);
		}
			else 
			{
				System.out.println("      Login 오류(재입력) ");
				UserSystem();
			}	
		}
		
		catch( Exception e) 
		{
			System.out.println("     캐치 오류 재실행");
			UserSystem();
		}
	}
				
	
		//d
		//아이디와 비번 한번 쿼리 번 
	
	//아이디를 가져가자
	public void UserSystem2(String IDFile) throws SQLException { 
		View title = new View();
		title.UserManu();
		
			
			while(true) {
				String Input = In.nextLine();
				int Acept =move.InsertCheck2(Input);
			
			switch(Acept) 
			{	
			case 1:
				 Bookloan(IDFile);
				break;
			case 2:
				Bookreturn(IDFile);
				break;
			case 3:
				UserFile(IDFile);
				break;
			case 4:
				LoginBack();
				break;
			case 5:
				End();
				break;
				// 꺁 낼때 클로즈 만륻어 ㅜ주ㅕㅁㄴ 편핮징 안나??
				
				
			default:
				System.out.print("항목에 없는 번호 입니다 재입력: ");
				break;	
				
		     	}
			}
     	}
	
	
	
	
	//  책 대출 제목 검색 
	public void Bookloan(String IDFile) throws SQLException 
	{
	 try {	
		   String BookNE=VW.BookNameInsert();
		   rs=Dbcon.UserLoanBookCheck(BookNE);
			
		   if(rs.next()) 
			{
			 rs.beforeFirst();
			 VW.BookNameList(rs);
			 Bookloan2(IDFile);
			}
			
			else 
			{
				System.out.println("      책 목록에 없는  제목 입니다 재입력 ");
				Bookloan(IDFile);
			}
		}
			
		catch(Exception e)
			{
			System.out.println(" 캐치 오류 ");
			Bookloan(IDFile);
			}
	}
	
	// 대출 책 ID 검색 / 대출하기 
	public void Bookloan2(String IDFile) throws SQLException 
	{	
			try {	
				String Number = VW.BookIdInsert();   //북 ID 입력 부분 
				rs=Dbcon.BookIdRepeatCheck(IDFile,Number); //회원이 입력한 북ID 중복 체크 
				if(rs.next()) 
				{
					System.out.println("\n      이미 대출 되있는 책입니다 재검색합니다 ");
					Bookloan(IDFile);
				}
				 	 	 
				rs=Dbcon.SearchkBookId(Number); //도서관에 회원이 찾는 책이 있는지 확인 
				  
				if(rs.next()) 
				{
				 String BN = rs.getString("bookname");
				 Dbcon.BookLoanCountUpdate(Number); //수량 하나 빼기 
				 SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
				 Date now=new Date();	
				 String Time= format1.format(now);
				 String State= "대출";	
				 Dbcon.InsertRentRecord(IDFile,Number,State,Time); //대출 기록 입력  /기록 남김 
				 System.out.print("     "+IDFile+"님  "+BN+" 책  대출완료  ");
				 }
				
				 
				 else {
						System.out.print("\n      존재하지 않은 책 ID 입니다 (오류 재 입력) ");
						Bookloan2(IDFile);
					  }
			}
			catch(Exception e) 
			{
				System.out.print("\n      22캐치오류 : ");
				Bookloan2(IDFile);
			}

			move.UserYN(IDFile);
			
			
			
	
	}
	
	public void Bookreturn(String IDFile) throws SQLException 
	{
		
		System.out.print("    ▶ "+IDFile+" 님의 대출목록  \n");
		rs=Dbcon.UserFileLoanRead(IDFile);
		 if(rs.next()) 
			{
			 rs.beforeFirst();
				while(rs.next()) {
					Book Bk= new Book();
					Bk.setBookID(rs.getString("bookid"));
					Bk.setBookName(rs.getString("bookname"));
					Bk.setBookWriter(rs.getString("writer"));
					BookListFile.add(Bk);
					}
				VW.UserRentBookListInformation(BookListFile);
			// title.BookNameList(rs);
			}
			else 
			{
				System.out.println("      대출한 책이 없습니다. ");
				move.UserYN(IDFile);
			}
		 
		 
		 
		 
		 while(true) {
				while(true) {
					System.out.print("     반납할 번호(No)입력 ");
					String Search= In.nextLine();
					
					int NumSearch=move.InsertCheck2(Search);
					
					//String Info=BookListFile.get(Integer.parseInt(NumSearch)).getBookID();
					
					if(NumSearch>BookListFile.size()||NumSearch<0) {
						System.out.println("     검색창에 없는 No입니다 재입력  ");
						
					}
						else {
						
						String Info=BookListFile.get(NumSearch).getBookID();
					
							
						
						rs=Dbcon.UserReturnCheck(IDFile,Info);
						

						if(rs.next()) {
							
							for(int Num=0; Num <BookListFile.size();Num++) {
								if	(NumSearch==Num) {
									BookListFile.remove(Num);										
										Dbcon.BookReturnCountUpdate(Info);	
										SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
										Date now=new Date();	
										String Time= (format1.format(now)+"반납완료");
										String State= "반납";
										
										Dbcon.UpdateRentRecord(IDFile,Info,State,Time);
										System.out.println("     "+IDFile+" 님 책 ID : "+Info+"  반납 완료");
										
										
										BookListFile.clear();
										move.UserYN(IDFile);	
										break;
								}
							}
						}
							else {
								System.out.println("     반납오류");
								
								}
				}
			break;		
			}			
		}
	}
		
		
		

		
	/*
	// 2 책 반납
	public void Bookreturn(String IDFile) throws SQLException 
	{
		
		System.out.print("     "+IDFile+" 님의 대출목록 : \n");
		rs=Dbcon.UserFileLoanRead(IDFile);
		VW.LoanRead(rs);
		
		
		
		int Num=0;
		rs=Dbcon.UserFileBookLoanCountRead(IDFile);
		VW.LoanCountRead(rs, Num,IDFile);
		
		try {
			
		System.out.print("     반납하실 책의 ID를 입력하세요 :");
		while(true) {
		
			String BookID= In.nextLine();
			rs=Dbcon.UserReturnCheck(IDFile,BookID);  //book ID 대출기록에 유무 체크 검사 
			
			if(rs.next()) 
			{
				Dbcon.BookReturnCountUpdate(BookID);	
				SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
				Date now=new Date();	
				String Time= (format1.format(now)+"반납완료");
				String State= "반납";
				
				Dbcon.UpdateRentRecord(IDFile,BookID,State,Time);
				System.out.println("     "+IDFile+" 님 책 ID : "+BookID+"  반납 완료");
				
				break;
			}
			
					else 
					{
						System.out.print("     대출 항목에 없는 잘못된 ID , 책의 ID를 재입력하세요 :");
					}
			}
			
		
		
		}
		catch(Exception e) {
			System.out.println("     한글 오류 캐치오류");
			Bookreturn(IDFile);	
		}
		move.UserYN(IDFile);
	}
		
	
	*/
	
	// 3 회원 정보
	public void UserFile(String IDFile) throws SQLException 
	{
		
		rs=Dbcon.UserFileRead(IDFile); //유저 정보 쿼리 읽기 
		VW.Basic(rs); // 보여주기 
		
		int Rset=0;
		rs=Dbcon.UserFileLoanRead(IDFile);//유저 대출 기록 읽기
		VW.LoanRead(rs); // 보여주기 
		
		rs=Dbcon.UserFileBookLoanCountRead(IDFile);//유저 대출 책 기록 수량 보여주기 
		VW.LoanCountRead(rs, Rset,IDFile); //값읽기 
		
		move.UserYN(IDFile);
	}
	
	
	
	// 4 초기 메뉴 돌아가기
	public void LoginBack() throws SQLException 
	{
		start.action();
	}
	
	// 5 종료
	public void End() 
	{
		System.out.println("     종료합니다.");
		System.exit(0);
	}
	
	
	
	
	
	/*
	Connection con =DriverManager.getConnection(url, id, password);
	Statement stmt = con.createStatement(); //쿼리를 DB에 보내기 위한 객체
	ResultSet rs=null;
	
	String sql="select * from membr where membrid = 'aa'";

	rs=stmt.executeQuery(sql);
	System.out.print(rs.next());
	try{ 
	}catch (Exception e){
			System.out.print("시스템 에러야");
	}//최상위 클래스가 아니라면 무조건 던져주자
	
	/*
	while(rs.next()) {
		System.out.print((rs.getString("memberid") + " "));
	}
	*/
	
	
}
