package newDoser.View;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import newDoser.Control.Move;
import newDoser.Model.Book;
import newDoser.Model.User;

public class View
{
	private int BackID;
	private int BackPW;
	
	User Ur=new User();
	
	ArrayList<Book> BookLiseFile = new ArrayList<Book>();
	ArrayList<User> UserListFile = new ArrayList<User>();
	

	Scanner In =new Scanner(System.in); 
	
	
	// 바탕 틀 화면 ==================================================================
	
	public void Title() {
		System.out.println("      -도서관리 프로그램- \n");
		System.out.println("    [1] 로그인\n");
		System.out.println("    [2] 회원가입\n");
		System.out.println("    [3] 관리자모드\n");
		System.out.println("    [4] 종료");
		System.out.print("\n       번호 선택 :");
	}
	

	public void UserManu() {
		System.out.println("      -회원메뉴- \n");
		System.out.println("    [1] 책 검색/대출 \n");
		System.out.println("    [2] 책 반납\n");
		System.out.println("    [3] 회원 정보\n");
		System.out.println("    [4] 초기 메뉴 돌아가기\n");
		System.out.println("    [5] 종료");
		System.out.print("\n       번호 선택 :");
	}
	
	public void ManagerManu() {
		System.out.println("      -관리자 메뉴- \n");
		System.out.println("    [1] 회원 리스트 \n");
		System.out.println("    [2] 책 리스트\n");
		System.out.println("    [3] 회원 검색\n");
		System.out.println("    [4] 회원 삭제\n");
		System.out.println("    [5] 신규 책 등록\n");
		System.out.println("    [6] 책 삭제\n");
		System.out.println("    [7] 책 대여,반납 기록\n");
		System.out.println("    [8] 초기메뉴 돌아가기");
		System.out.print("\n       번호 선택 :");
	}
	
	
	// 로그인 항목 ===================================================================
	
	
	public String LogID() {
		System.out.print("      ID 입력: ");
		String ID= In.nextLine();
		return ID;
	}
	
	public String LogPW()
	{
		System.out.print("      pW 입력: ");
		String PW= In.nextLine();	
		return PW;
	}
	
	
	// 유저 기본정보 보여주기 
	public void Basic(ResultSet rs) throws SQLException{ 
	
	if(rs.next()) {
		System.out.println("User ID : "+rs.getString("memberid"));
		System.out.println("User Name : "+rs.getString("membername"));
		
		}
	}
	
	//리스트 회원 정보 보여주기 
	public void UserInformation(List<User> test){
		
		for(int Num=0; Num <test.size();Num++) {
			
			System.out.println("회원 ID : "+test.get(Num).getMemberID());
			System.out.println("회원 password : "+test.get(Num).getPassword());
			System.out.println("회원 Name : "+test.get(Num).getMembername());
		}		
	}
	
	public void UserListInformation(List<User> test){
		
		for(int Num=0; Num <test.size();Num++) {
			System.out.println("   No: "+Num);
			System.out.println("회원 ID : "+test.get(Num).getMemberID());
			System.out.println("회원 password : "+test.get(Num).getPassword());
			System.out.println("회원 Name : "+test.get(Num).getMembername());
		}		
	}
	
	
	
	//리스트 책 정보 보여주기 
		public void BookInformation(List<Book> test){
			
			for(int Num=0; Num <test.size();Num++) {
				
				System.out.println("책 ID : "+test.get(Num).getBookID());
				System.out.println("책 제목 : "+test.get(Num).getBookName());
				System.out.println("책 저자 : "+test.get(Num).getBookWriter());
				System.out.println("책 수량 : "+test.get(Num).getBookAmount());
			}		
		}
	
	
	public void BookListInformation(List<Book> test){
				
				for(int Num=0; Num <test.size();Num++) {
					System.out.println("   ▶ No: "+Num);
					System.out.println("   책 ID : "+test.get(Num).getBookID());
					System.out.println("   책 제목 : "+test.get(Num).getBookName());
					System.out.println("   책 저자 : "+test.get(Num).getBookWriter());
					System.out.println("   책 수량 : "+test.get(Num).getBookAmount());
				}		
			}
		
	

	public void UserRentBookListInformation(List<Book> test){
				
				for(int Num=0; Num <test.size();Num++) {
					System.out.println("  ▶  No: "+Num);
					System.out.println("   책 ID : "+test.get(Num).getBookID());
					System.out.println("   책 제목 : "+test.get(Num).getBookName());
					System.out.println("   책 저자 : "+test.get(Num).getBookWriter());
					
				}		
			}
	
	
	
	// 회원 대출 기록 보여주기 
	public void LoanRead(ResultSet rs) throws SQLException{ 
	while( rs.next()) {
	System.out.println("대여 책  ID : "+rs.getString("bookid")+" 책이름 : " +rs.getString("bookname")+"  책 출판사  : "+rs.getString("writer") );
		}
	}
	
	
	
	
	//회원 책 수량 보여주기 
	public void LoanCountRead(ResultSet rs,int Num,String IDFile) throws SQLException{ 
		int Rset=Num;
		if(rs.next()) {
			Rset=rs.getInt(1);
			 System.out.println("대여 책  권수 : "+Rset);
			 if(Rset==0) {
				 Move move=new Move();
				 System.out.print("  대출한 책이 없습니다 ");
				 move.UserYN(IDFile);
			 }
			 
		}
		else { System.out.println("대여 책  없음 ");}
	}
	
	//삭제
	public String DeleteBookNameInsert() {
		System.out.print("     삭제할 책 제목 검색 : ");
		String BookNE= In.nextLine();
		return BookNE;
	}
	
	
	
	//대출 책 제목 입력하기 
	public String BookNameInsert() {
		System.out.print("      책 제목 검색 : ");
		String BookNE= In.nextLine();
		return BookNE;
	}
	
	
	
	
	//대출 책 Id 입력하기  
	public String BookIdInsert() {
		System.out.print("\n      대출하실 책  ID 입력 : ");
		String Number= In.nextLine();
		return Number;
	}
	
	
	
	public void BookNameList(ResultSet rs) throws SQLException {
		while(rs.next()) {
		System.out.println(" 책 ID :"+rs.getInt("bookid"));
		System.out.println(" 책 이름 :"+rs.getString("bookname"));
		System.out.println(" 책 저자 :"+rs.getString("writer"));
		System.out.println(" 책 수량 :"+rs.getInt("count")+"\n");
		}
	}
	
	
	//관리자 항목 ==================================================================== 
	
	
	
	
	
	
	
	
	
	
	
	//책등록 
	
	
	//책  ID 등록 
	public String NewBookID() {
		
		System.out.print("     신규 책 ID 입력: ");
		String Input = In.nextLine();
		return Input;
	}
	
	public String NewBookName() {
			
		System.out.print("     신규 책 제목 입력: ");
		String Input = In.nextLine();
		return Input;
		}
		
	public String NewBookWriter() {
		
		System.out.print("     신규 책 저자 입력: ");
		String Input = In.nextLine();
		return Input;
	}

	
	public void NewBookCount() {
		System.out.print("     신규 책 수량 입력: ");
		
	}
	
	
	//  화면 돌아가기 항목 ==============================================================
	
	
	
	

	
	public void BackUserManu() {
		System.out.println("\n      - BACK -   \n");
		System.out.println("\n      ▶ 로그인 메뉴 돌아가기 :y  \n");
		System.out.println("        ▶ 프로그램 종료 :n  \n");
		System.out.print("        y or n 입력 : ");
		
	}	
	
	
	public void BackManagerManu() {
		
		System.out.println("\n      - BACK -   \n");
		System.out.println("      - 관리자 메뉴 돌아가기 :y - \n");
		System.out.println("      -  메인화면으로 가기 :n - \n");
		System.out.print("      y or n 입력 : ");
		
	}
	
	public String JoinAfterBack() {
		System.out.println("\n      - BACK -   \n");
		System.out.println("      - 메인 메뉴 돌아가기 :y - \n");
		System.out.println("      -   프로그램 종료 :n - \n");
		System.out.print("      y or n 입력 : ");
		String Number= In.nextLine();
		return Number;
	}	
	
	
}


/*
class BackResuit{
	private int BackID;
	private int BackPW;
	
	
	public BackResult(String BackID,String BackPW) {
		this.BackID= BackID;
		this.BackPW= BackPW;
	}
	
	public String LogInsertID() {
		return BackID;
	}
	public String LogInsertPW() {
		return BackPW;
	}
	
	public static test() {
		String a="23";
		String b="34";
		return BackResult(a,b);
	}

}
*/

