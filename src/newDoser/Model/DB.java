package newDoser.Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import newDoser.View.View;

public class DB {
	View title = new View();
	
	Scanner In =new Scanner(System.in); 
	String url = "jdbc:mariadb://localhost:3307/test";
	String id = "root";
	String password = "1122";
	Statement stmt = null;


	

	
	
	// DB연동 부분  Stmt 중복 ====================================================================================================
	public Statement DBconnec() throws SQLException {
	Connection con =DriverManager.getConnection(url, id, password);
	Statement Stmt = con.createStatement();
	return Stmt;	
	}
	
	
	
	
	//회원 기능 항목 ==============================================================================================
	
   //로그인 부분 
	
	public ResultSet LogCheck(String ID,String PW) throws SQLException {
	stmt=DBconnec();
	String sql="select memberid,password from member where memberid='"+ID+"' and password='"+PW+"'";
	ResultSet rs=stmt.executeQuery(sql);
	return rs;
	}
	
	
	
	//▶▶▶▶회원 가입  하기 
	
	//회원 등록 부분 
	public void LogInComplete(String id,String password,String Con) throws SQLException {
		stmt=DBconnec();
		String sql="insert into member(memberid,password,membername) values('"+id+"','"+password+"','"+Con+"')";
		stmt.execute(sql);
	}
	
	
	
	// 회원 책 검색/대출  쿼리 
	
	//▶▶▶▶ 책 제목 검색 
	
	public ResultSet UserLoanBookCheck(String BookNE)throws SQLException {
	stmt=DBconnec();
	String sql="select * from book where bookname Like'%"+BookNE+"%'";
	ResultSet rs = stmt.executeQuery(sql);
	return rs;
	 }
	
	
	
	
	//▶▶▶▶중복 대출 체크 
	 public ResultSet BookIdRepeatCheck(String IDFile,String Num) throws SQLException {
		 stmt=DBconnec();
		 String Number= Num;
		 String sql = "select bookid from renthistory where bookid='"+Number+"' and memberid ='"+IDFile+"'";
		 ResultSet rs = stmt.executeQuery(sql);
	 return rs;
	}
	
	
	//▶▶▶▶ 입력한 ID 책 있는지 없는지 확인 하기 
	 public ResultSet SearchkBookId(String Num) throws SQLException {
		 stmt=DBconnec();
		 //String Number= Num;
		 String sql = "select * from book where bookid='"+Num+"'";
		 ResultSet rs = stmt.executeQuery(sql);
	 return rs;
	 }
	 
	 
	 //▶▶▶▶대출할시 수량 하나 빼기 
	 public void BookLoanCountUpdate(String Num) throws SQLException {
	     stmt=DBconnec();
	     String Number= Num;
	     String sql = "update book set count=count-1 where bookid='"+Number+"'";
		 stmt.executeUpdate(sql);
	 }
	 
	 
	 
	 public void InsertRentRecord(String IDFile,String Num,String StateInfo,String TimeInfo) throws SQLException {
		 stmt=DBconnec();
		 String sql = "insert into renthistory(bookid,memberid,state,date) values('"+Num+"','"+IDFile+"','"+StateInfo+"','"+TimeInfo+"')";
			  
		 stmt.executeUpdate(sql);
	 }
	 
	 
	 
	 
	 public void UpdateRentRecord(String IDFile,String Num,String StateInfo,String TimeInfo) throws SQLException {
		 stmt=DBconnec();
		//String sql = "insert into renthistory(bookid,memberid,state,date)values('"+Num+"','"+IDFile+"','"+StateInfo+"','"+TimeInfo+"')";
		 String sql = "update renthistory set state='"+StateInfo+"',back='"+TimeInfo+"'where memberid='"+IDFile+"'and bookid='"+Num+"'";
		 stmt.executeUpdate(sql);
	 }
	 
	 
	 
	//[2]회원 책 반납 쿼리 
	
	 public ResultSet UserReturnCheck(String IDFile,String test) throws SQLException {
     stmt=DBconnec();
	 //String sql="select r.bookid from book b,renthistory r where b.bookid = r.bookid and r.memberid ='"+IDFile+"' and r.bookid='"+test+"'";
	 String sql="select * from renthistory where memberid='"+IDFile+"' and bookid='"+test+"'";
	 ResultSet rs = stmt.executeQuery(sql);
	 return rs;
	 }
	
	 //"select * from book b,renthistory r where b.bookid = r.bookid and r.memberid ='"+IDFile+"'";
	
	 public void BookReturnCountUpdate(String BookID) throws SQLException {
	 stmt=DBconnec();
	 String sql = "update book set count=count+1 where bookid='"+BookID+"'";
	 stmt.executeUpdate(sql);
}
	
	
	
	
	
	
	//  [3] 회원 정보 확인하기
	
	//▶▶▶▶회원 멤버기본 정보  읽기
	public ResultSet UserFileRead(String IDFile) throws SQLException {
		
		stmt=DBconnec();
		String sql="select * from member where memberid='"+IDFile+"'";
		ResultSet rs = stmt.executeQuery(sql);
		return rs;
	}
	
	
	// ▶▶▶▶회원 대출 기록 확인하기 
	public ResultSet UserFileLoanRead(String IDFile) throws SQLException {
		stmt=DBconnec();
		String state="대출";
		String sql="select * from book b,renthistory r where b.bookid = r.bookid and r.memberid ='"+IDFile+"' and r.state='"+state+"'";
		ResultSet rs = stmt.executeQuery(sql);
		return rs;
	}
	
	
	
	//▶▶▶▶회원 파일 책 빌린수량 읽기 
	public ResultSet UserFileBookLoanCountRead(String IDFile) throws SQLException {
		stmt=DBconnec();
		String state="대출";
		String sql="select count(*) from renthistory where memberid='"+IDFile+"' and state='"+state+"'";
		ResultSet rs = stmt.executeQuery(sql);
		return rs;
	}
	
	
	
	//관리자 항목 =========================================================================== 
	
	//관리자 로그인
	public ResultSet  ManagerLoginCheck(String Log,String password) throws SQLException {
		stmt=DBconnec();
		String sql="select password from member where memberid='"+Log+"' and password='"+password+"'";
		ResultSet rs=stmt.executeQuery(sql);
		return rs;
	}
	
	
	//회원 전체 목록 환인 	
	public ResultSet UserFileCheck() throws SQLException {
		stmt=DBconnec();
		String sql="select * from member";
		ResultSet rs = stmt.executeQuery(sql);
		return rs;	
	}
	
	
	//회원 전체 책 목록
	public ResultSet BookFileCheck() throws SQLException {
		stmt=DBconnec();
		String sql="select * from book";
		ResultSet rs = stmt.executeQuery(sql);
		return rs;	
	}
	
	
	//삭제하기 전  회원 검색 
	public ResultSet UserFileSearch(String Search) throws SQLException {
		stmt=DBconnec();
		String sql="select * from member where memberid like'%"+Search+"%'";
		ResultSet rs = stmt.executeQuery(sql);
		return rs;	
	}
	
	//삭제할 아이디 검색 
	public ResultSet UserIdDeleteSearch(String Search) throws SQLException {
		stmt=DBconnec();
		String sql="select memberid from member where memberid='"+Search+"'";
		ResultSet rs = stmt.executeQuery(sql);
		return rs;	
	}
	
	//선택한 Id 삭제 하기 
	public void UserIdFileDeleteh(String Search) throws SQLException {
		stmt=DBconnec();
		String sql="delete from member where memberid='"+Search+"'";
		stmt.executeUpdate(sql);
	}
	
	
	
	
	
	
	
	public ResultSet ResultSet (String Input) throws SQLException {
		stmt=DBconnec();
		String sql="select bookid from book where bookid='"+Input+"'";
		ResultSet rs = stmt.executeQuery(sql);
		return rs;
	
	}
	
	
	//삭제하기전 대출한 책이 있는지 유무 검사 
	public ResultSet DeleteCheck (String IdInput) throws SQLException {
		stmt=DBconnec();
		String state="대출";
		String sql="select state from renthistory where memberid='"+IdInput+"' and state='"+state+"'";
		ResultSet rs = stmt.executeQuery(sql);
		return rs;
	}
	
	
	//책 삭제하기전 대출한 책 목록 있는지 검사 
	public ResultSet BookDeletebeforeCheck (String Search,String state) throws SQLException {
		stmt=DBconnec();
		String sql="select bookid from renthistory where bookid='"+Search+"' and state='"+state+"'";
		ResultSet rs =stmt.executeQuery(sql);
		return rs;
	}
	
	
	//선택한 책 삭제 
	public void BookIdFileDeleteh(String Search) throws SQLException {
		stmt=DBconnec();
		String sql="delete from book where bookid='"+Search+"'";
		stmt.executeUpdate(sql);
	}
	
	
	
	// 책등록 
	public void BookAdd(int Input,String Input2,String Input3,int Input4) throws SQLException {
		stmt=DBconnec();
		String sql="insert into book(bookid,bookname,writer,count) values('"+Input+"','"+Input2+"','"+Input3+"','"+Input4+"')";
		stmt.executeUpdate(sql);
	}

	//대출 반납 기록 확인하기 
	public ResultSet renthistoryRead () throws SQLException {
		stmt=DBconnec();
		String sql="select * from renthistory";
		ResultSet rs = stmt.executeQuery(sql);
		return rs;
	}
	
	
}