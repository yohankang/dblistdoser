package newDoser;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import newDoser.Control.Move;
import newDoser.Model.Book;
import newDoser.Model.DB;
import newDoser.Model.User;
import newDoser.View.View;


public class Manager {
	View title = new View();
	DB Dbcon =new DB();
	Move move =new Move();
	Book Bk= new Book();
	User Ur=new User();
	
	
	Scanner in = new Scanner(System.in);
	Scanner In =new Scanner(System.in); 
	
	
	ArrayList<Book> BookListFile = new ArrayList<Book>();
	ArrayList<User> UserListFile = new ArrayList<User>();
	

	ResultSet rs=null;
	
	public void ManagerSystemLogin() throws SQLException 
	
	{
		// 관리자모드 예외 처리 한글 영어 문자 
		
		Scanner In =new Scanner(System.in); 		
		String Log=null;
		Log="admin";
		System.out.print("       관리자 비밀번호 입력 : ");
		String password = In.nextLine();
		try {
			rs=Dbcon.ManagerLoginCheck(Log,password);
			
			if(rs.next())
			{
				ManagerSystem();
			}
			else 
			{
				System.out.println("       비밀번호가 일치하지 않습니다 재입력");
				ManagerSystemLogin();
			}
		}
		catch(Exception e) 
		{
			System.out.println("       캐치 오류(한글 등) 재입력");
			ManagerSystemLogin();
		}
	}
	
	
	public void ManagerSystem() throws SQLException 
	{
		
		title.ManagerManu();	
		while(true) {
			String Input = in.nextLine();
			int Acept =move.InsertCheck2(Input);
						
				switch(Acept) 
				{
				case 1:
					UserList();
					break;
				case 2:
					BookList();
					break;
				case 3:
					UserSearch();
					break;
				case 4:
					UserDelete();
					break;
				case 5:
					BookADD();	
					break;
				case 6:
					BookDelete();
					break;
				case 7:
					BookRecord();
					break;
				case 8:
					Back();
					break;
					
				default:
					System.out.print("항목에 없는 번호 입니다 재입력: ");
					break;
			    }
			}
		}  		
	
	
	

	// 1 유저리스트
	public void UserList() throws SQLException 
	{
		System.out.println(" - 회원 리스트 - ");
		
		
		rs=Dbcon.UserFileCheck();
		
		if(rs.next()) 
		{
			 rs.beforeFirst();
			while(rs.next()) 
			{
				// DB읽은 파일 리스트에 추가 하기 
				User Ur=new User();
				Ur.setMemberID(rs.getString("memberid"));
				Ur.setPassword(rs.getString("password"));
				Ur.setMembername(rs.getString("membername")+"\n");
				 UserListFile.add(Ur);
			}
			//리스트 출력하기
			title.UserInformation(UserListFile);
			UserListFile.clear();
		}
		
		
		else 
		{
			System.out.println("      등록된 유저가 없습니다.  ");	
		}
		move.ManagerYN();
		
	}
	
	
	/*
	
	// 1유저리스트
	public void UserList() throws SQLException 
	{
		System.out.println(" - 회원 리스트 - ");
		
		//String sql="select * from member";
		//rs = stmt.executeQuery(sql);
		rs=Dbcon.UserFileCheck();
		
		if(rs.next()) 
		{
			 rs.beforeFirst();
			while(rs.next()) 
			{
				System.out.println(" 유저 ID :"+rs.getString("memberid"));
				System.out.println(" 유저 password :"+rs.getString("password"));
				System.out.println(" 이름 :"+rs.getString("membername")+"\n");
			}
		}
		else 
		{
			System.out.println("      등록된 유저가 없습니다.  ");	
		}
		move.ManagerYN();	
	}
	*/
	
	
	// 2 책 리스트
	public void BookList() throws SQLException 
	{
		
		
		rs=Dbcon.BookFileCheck();
		
		if(rs.next()) 
		{
			 rs.beforeFirst();
			while(rs.next()) 
			{
				// DB읽은 파일 리스트에 추가 하기 
				Book Bk= new Book();
				Bk.setBookID(rs.getString("bookid"));
				Bk.setBookName(rs.getString("bookname"));
				Bk.setBookWriter(rs.getString("writer"));
				Bk.setBookAmount(rs.getString("count")+"\n");
				BookListFile.add(Bk);
			}
			title.BookInformation(BookListFile);
			BookListFile.clear();
		}
		
		else 
		{
			System.out.println("      책 목록이 비어있습니다. ");
		}
		
		move.ManagerYN();
	}
	
	
	
	// 2 책 리스트
	/*
	public void BookList() throws SQLException 
	{
		
		
		rs=Dbcon.BookFileCheck();
		
		if(rs.next()) 
		{
			 rs.beforeFirst();
			while(rs.next()) 
			{
				
				System.out.println(" 책 ID :"+rs.getInt("bookid"));
				System.out.println(" 책 이름 :"+rs.getString("bookname"));
				System.out.println(" 책 저자 :"+rs.getString("writer"));
				System.out.println(" 책 수량 :"+rs.getInt("count")+"\n");
			}
		}
		
		else 
		{
			System.out.println("      책 목록이 비어있습니다. ");
		}
		
		move.ManagerYN();
	}
	*/
	
	
	
	// 3 회원검색
	public void UserSearch() throws SQLException 
	{
		
		
		System.out.println("      검색할 사용자 ID 입력 ");
		String Search= In.nextLine();
		
		
		rs=Dbcon.UserFileSearch(Search);
		
		if(rs.next()) 
		{
			 rs.beforeFirst();
			while(rs.next()) 
			{
				System.out.println(" 유저 ID :"+rs.getString("memberid"));
				System.out.println(" 유저 password :"+rs.getString("password"));
				System.out.println(" 이름 :"+rs.getString("membername")+"\n");
			}
		}
			else 
			{
				System.out.println("      없는 ID 입니다 재입력 ");
				 UserSearch();
			}
			
		move.ManagerYN();
	}
	
	
	// 4 회원삭제
	public void UserDelete() throws SQLException 
	{
	
		while(true) {
			System.out.println("      삭제할 사용자 ID 입력 ");
			String Search= In.nextLine();
			
			
			rs= Dbcon.UserFileSearch(Search);
			
			if(rs.next()) 
			{
				 rs.beforeFirst();
				while(rs.next()) 
				{
					User Ur=new User();
					Ur.setMemberID(rs.getString("memberid"));
					Ur.setPassword(rs.getString("password"));
					Ur.setMembername(rs.getString("membername")+"\n");
					 UserListFile.add(Ur);
				}
				//리스트 출력하기
				title.UserListInformation(UserListFile);
				break;
			}
			
			else 
			{
				System.out.println("      회원 ID가 존재하지 않습니다  재입력 ");
				
			}
		}
		
		
		
		
		while(true) {
			
		
			while(true) {
				System.out.print("     삭제할 번호 입력 ");
				String Search= In.nextLine();
		
				int NumSearch=move.InsertCheck2(Search);
				
				if(NumSearch>UserListFile.size()||NumSearch<0) {
					System.out.println("     검색창에 없는 No입니다 재입력  ");
					
				}
				
				else {
				String Info=UserListFile.get(NumSearch).getMemberID();
				//예는 어떻게 예외 처리 하지????????????????????//
				//리스트에 있는 번호만 가야한는데 없는 번호이다 
				
	
			rs=Dbcon.UserIdDeleteSearch(Info);
			if(rs.next()) {
				rs.beforeFirst();
				
				rs=Dbcon.DeleteCheck(Info);//유저 대출 책 기록  여부확
				if(rs.next()) {
					System.out.println("     회원  "+Info+" 님은 반납하지 않은 책이 남아있습니다.(삭제 불가) ");
					UserDelete();
				}
				
					else {
						if(Info.equals("admin")) {
							System.out.println("     관리자 입니다 삭제가 불가능합니다.  ");
							move.ManagerYN();	
						}
							else {
								for(int Num=0; Num <UserListFile.size();Num++) {
									if	(NumSearch==Num) {
										
										UserListFile.remove(Num);
										Dbcon.UserIdFileDeleteh(Info);
										System.out.println("     삭제완료 ");
										UserListFile.clear();
										move.ManagerYN();	
										break;
										
										}
									}
								}
					}
			}
			else {
				System.out.println("     존재하지 않은 ID 입니다 재입력 ");
				 }
		}
	}
			
	}
}
		/*
		System.out.println("      삭제할 사용자 ID 입력 ");
		String Search= In.nextLine();
		
		
		rs= Dbcon.UserFileSearch(Search);
		
		if(rs.next()) 
		{
			 rs.beforeFirst();
			while(rs.next()) 
			{
				System.out.println(" 유저 ID :"+rs.getString("memberid"));
				System.out.println(" 유저 password :"+rs.getString("password"));
				System.out.println(" 이름 :"+rs.getString("membername")+"\n");
			}
		}
			else 
			{
				System.out.println("      없는 ID 입니다 재입력 ");
				 UserSearch();
			}
			
		// 회원 검색 기능 추가 하기 	
		try 
		{
			
			
			System.out.print("     삭제할 사용자 ID 입력 ");
			 Search= In.nextLine();
			
			
			rs=Dbcon.UserIdDeleteSearch(Search);
			
			
			if(rs.next()) {
				 rs.beforeFirst();
				
				rs=Dbcon.DeleteCheck(Search);//유저 대출 책 기록  여부확
				if(rs.next()) {
					System.out.println("     회원  "+Search+" 님은 반납하지 않은 책이 남아있습니다.(삭제 불가) ");
					UserDelete();
				}
				
				else {
					if(Search.equals("admin")) {
						System.out.println("     관리자 입니다 삭제가 불가능합니다.  ");
						UserDelete();
					}
					else {
					Dbcon.UserIdFileDeleteh(Search);
					System.out.println("     삭제완료 ");
					}
				}
				
				
			}
			else {
				System.out.println("     존재하지 않은 ID 입니다 재입력 ");
				UserDelete();
			}
			
			
		}
			catch(Exception e)
			{
				System.out.println("     99존재하지 않은 ID 입니다 재입력 ");
				UserDelete();
			}
			
		move.ManagerYN();
		
	
	}
		*/
		
	// 5 책 추가 
	public void BookADD() throws SQLException 
	{
		
		System.out.println("     -신규 책 등록-");
		
		String Input="";
		
		Input =move.TTTBookIDInsert();
		Input=move.TTTnewBookIdInsert(Input);
		
		
		String Input2= move.NewBookNameEnter();
		
		String Input3=move.NewBookWriterEnter();
		
		String Input4=move.BookCountInsert();
		int Count=move.BookCountAmountCheck(Input4);
		
		//String sql="insert into book(bookid,bookname,writer,count) values('"+Integer.parseInt(Input)+"','"+Input2+"','"+Input3+"','"+Count+"')";
		Dbcon.BookAdd(Integer.parseInt(Input),Input2,Input3,Count);
		//if(stmt.executeUpdate(sql)>0) {
		
		System.out.println("     책 등록 성공  ");
	
		move.ManagerYN();
	}
	
	// 6 책 삭제
	public void BookDelete() throws SQLException 
	{
		
		while(true) {
		String BookNE=title.DeleteBookNameInsert();
		   rs=Dbcon.UserLoanBookCheck(BookNE);
			
		   if(rs.next()) 
			{
			 rs.beforeFirst();
				while(rs.next()) {
					Book Bk= new Book();
					Bk.setBookID(rs.getString("bookid"));
					Bk.setBookName(rs.getString("bookname"));
					Bk.setBookWriter(rs.getString("writer"));
					Bk.setBookAmount(rs.getString("count")+"\n");
					BookListFile.add(Bk);
					}
				title.BookListInformation(BookListFile);
			// title.BookNameList(rs);
			break;
			}
			
			else 
			{
				System.out.println("      책 목록에 없는  제목 입니다 재입력 ");
				
			}
	    }
		
		
		//System.out.print("     삭제할 책 ID 입력 ");
		
		
		//Id 한글 문자 예외처리 
		while(true) {
			
			
			
			while(true) {
				System.out.print("     삭제할 번호(No)입력 ");
				String Search= In.nextLine();
				
				int NumSearch=move.InsertCheck2(Search);
				
				//String Info=BookListFile.get(Integer.parseInt(NumSearch)).getBookID();
				
				if(NumSearch>BookListFile.size()||NumSearch<0) {
					System.out.println("     검색창에 없는 No입니다 재입력  ");
					
				}
					else {
					
					String Info=BookListFile.get(NumSearch).getBookID();
					//예는 어떻게 예외 처리 하지????????????????????//
					//리스트에 있는 번호만 가야한는데 없는 번호이다 
					
					
					
					String state ="대출";
					rs=Dbcon.BookDeletebeforeCheck(Info,state); 
					
					if(rs.next()) {
						System.out.println("     입력하신 책은 미반납된 책이 있습니다 (삭제 불가) ");
						
					}
					
					else if(NumSearch>BookListFile.size()) {
						System.out.println("     검색에 없는 No입니다 (삭제 불가) ");
					}
						else {
							//
							for(int Num=0; Num <BookListFile.size();Num++) {
							if	(NumSearch==Num) {
								
							Dbcon.BookIdFileDeleteh(Info); 
							BookListFile.remove(Num);
							System.out.println("     삭제완료 ");
							BookListFile.clear();
							move.ManagerYN();	
							break;
							}
						}
							
					}
					break;
				}
			}//2번째 while
		}
		
		
		/*
		
		try {
			rs=Dbcon.SearchkBookId(Search);
			if(rs.next()) {
				
				rs.beforeFirst();
				String state ="대출";
				rs=Dbcon.BookDeletebeforeCheck(Search,state); 
				if(rs.next()) {
					System.out.println("     입력하신 책은 미반납된 책이 있습니다 (삭제 불가) ");
					BookDelete();
				}
				
				else 
				{	
					Dbcon.BookIdFileDeleteh(Search); 
					System.out.println("     삭제완료 ");
				}
				
			}
			else {
				System.out.println("     존재하지 않은 책 ID 입니다 재입력  ");
				BookDelete();
			}
		}
			
		catch(Exception e)
		{
		
			System.out.println("     캐치 존재하지 않은 책입니다  책 ID 재입력 ");
			 BookDelete();
		}
		
		move.ManagerYN();
		*/
		
	}
	// 7 책 대여/반납기록 
	public void BookRecord() throws SQLException 
	{
		
		 Dbcon.renthistoryRead();
		if(rs.next()) 
		{
			rs.beforeFirst();
			while(rs.next()) 
			{
				if(rs.getString("back")==null) {
					System.out.println(rs.getString("date")+"  유저 ID: "+rs.getString("memberid")+" 님이  책ID: "+rs.getString("bookid") +" 을  대출함  ");
				}
				else {
				System.out.println(rs.getString("date")+"  유저 ID: "+rs.getString("memberid")+" 님이  책ID: "+rs.getString("bookid") +" 을  대출함  "+rs.getString("back"));
				}
			}
		}
		
		else {
			
			System.out.println(" 대출 반납 기록이 없습니다");
		}
			
		move.ManagerYN();
	}
	
	// 8 돌아가기
	public void Back() throws SQLException 
	{
		Start start = new  Start ();
		start.action();
	}
	
	
}
